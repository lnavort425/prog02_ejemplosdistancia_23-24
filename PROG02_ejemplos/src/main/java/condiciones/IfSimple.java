/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package condiciones;

/**
 *
 * @author luisnavarro
 */
public class IfSimple {

    public static void main(String[] args) {
        int valor = 0;
        System.out.println("El programa está ejecutándose."); // Esta línea se ejecuta en cualquier caso
        if (valor < 0) {
            System.out.println("El valor es negativo."); // Esta línea solo se ejecuta si valor < 0
        }
        valor = 0; // Esta línea se ejecuta en cualquier caso
        System.out.println("El valor ha sido reseteado a cero."); // Esta línea se ejecuta en cualquier caso
        System.out.println("El programa sigue ejecutándose."); // Esta línea se ejecuta en cualquier casoﬁﬁ
        
        
        //
        
        if (valor % 2 == 0) {  // Si el resto de la división entera entre 2 es cero, el número es par
    System.out.println("El número es par.");
} else { // Si no, el número es impar
    System.out.println("El número es impar.");
} 
    }
}
