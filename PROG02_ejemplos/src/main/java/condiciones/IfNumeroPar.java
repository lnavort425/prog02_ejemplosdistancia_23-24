/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package condiciones;

import java.util.Scanner;

/**
 *
 * @author luisnavarro
 */
public class IfNumeroPar {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int numero;
        System.out.print("Introduzca un número entero: ");
        numero = teclado.nextInt();

// Segunda parte: procesamiento (comprobación) y salida de resultados (mensaje por pantalla si es par)
        if (numero % 2 == 0) {  // Si el resto de la división entera entre 2 es cero, el número es par
            System.out.println("El número es par.");
        }
        
        if ( numero % 2 == 0 ) 
    if ( numero > 100 ) 
        System.out.println ("El número es par y superior a 100.");
    }

}
