/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bucles;

/**
 *
 * @author atecaiestrassierra
 */
public class Factorial {
    public static void main(String[] args) {
        int factorial=1; //acumular
        int numero=7; //7!=7*6*5*4*3*2*1;
        for(int i=7; i>0; i--) 
            factorial*=i;
        System.out.println(""+numero+"!="+factorial);
    }
}
