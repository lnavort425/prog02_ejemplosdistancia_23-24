/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bucles;

import java.util.Scanner;

/**
 *
 * @author luisnavarro
 */
public class MultiplosTresDeInicioAFin {

    public static void main(String[] args) {
        // Declaración de variables
// ------------------------
        Scanner teclado = new Scanner(System.in);
        int inicio, fin;    // Entradas
        int secuencia;       // contador
        int cuentaMultiplos3=0; // CUENTA Multiplos de 3
        int sumaMultiplos3=0; // suma Multiplos de 3

// Entrada de datos
// ----------------
        do {
            System.out.print("Introduzca el inicio: ");
            inicio = teclado.nextInt();
            System.out.print("Introduzca el fin, que ha de ser mayor que el inicio: ");
            fin = teclado.nextInt();
        } while (inicio > fin);
// Procesamiento
// -------------
         // Iniciamos contador
        //;  // Iniciamos acumulador
        for (int i=inicio; i<fin;i++){}
        for(secuencia = inicio,cuentaMultiplos3 = 0;secuencia<=fin;secuencia++)
        {
            if (secuencia%3==0){
                cuentaMultiplos3++;
                sumaMultiplos3+=secuencia;
            }
        }
// Recorremos números desde inicio hasta fin
        cuentaMultiplos3 = 0;
        secuencia=inicio;
        while (secuencia <= fin) {
            if (secuencia % 3 == 0) // Si alguno es múltiplo de 3, lo sumamos al acumulador (lo "acumulamos")
            {
                cuentaMultiplos3 ++;//= secuencia;
                sumaMultiplos3+=secuencia;
            }
            secuencia++;  // Incrementamos el contador que va desde inicio hasta fin
        }

// Salida de resultados
// --------------------
        System.out.println("La suma de los múltiplos de 3 entre " + inicio + " y " + fin + " es " + cuentaMultiplos3 + ".");
        System.out.println();
    }
}
