/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bucles;

/**
 *
 * @author atecaiestrassierra
 */
public class DescontadorHasta0 {

    public static void main(String[] args) {
        // Declaración de variables
        int contador;

// No hay entrada de datos: tenemos toda la información que necesitamos
// Procesamiento y salida de resultados a la vez: cuenta atrás
        System.out.println("Cuenta atrás desde 100 hasta 0, de 10 en 10.");
        contador = 100; // Iniciamos el contador a 100
        while (contador >= 0) { // Mientras el contador se mayor o igual que cero seguimos iterando el bucle
            System.out.print(contador + " ");
            contador -= 10; // Vamos restando 10 en cada iteración
        }
        System.out.println();
        
        for (int contador2=100; contador2>=0; contador2-=10) 
               System.out.print(contador2 + " ");
        
    }
}
